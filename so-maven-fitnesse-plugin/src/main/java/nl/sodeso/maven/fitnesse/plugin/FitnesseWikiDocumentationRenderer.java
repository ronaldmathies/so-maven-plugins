package nl.sodeso.maven.fitnesse.plugin;

import org.markdown4j.Markdown4jProcessor;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;

/**
 * @author Ronald Mathies
 */
public class FitnesseWikiDocumentationRenderer {

    private static final String DOCUMENT_BEGIN = "DOCUMENTATION-BEGIN";
    private static final String DOCUMENT_TESTCASE = "DOCUMENTATION-TESTCASE";
    private static final String DOCUMENT_LINK = "DOCUMENTATION-LINK";
    private static final String DOCUMENT_PURPOSE = "DOCUMENTATION-PURPOSE";
    private static final String DOCUMENT_END = "DOCUMENTATION-END";

    private static Markdown4jProcessor processor = new Markdown4jProcessor();

    public static WikiPage render(File file) throws IOException {
        WikiPage wikiPage = new WikiPage();

        StringBuilder unparsedDocumentation = new StringBuilder();

        boolean include = false;

        BufferedReader bufferedReader = Files.newBufferedReader(file.toPath(), Charset.forName("UTF-8"));
        String line = null;
        while ((line = bufferedReader.readLine()) != null) {
            if (line.startsWith(DOCUMENT_TESTCASE)) {
                wikiPage.setTestcase(line.substring(DOCUMENT_TESTCASE.length(), line.length()).trim());
                continue;
            }
            if (line.startsWith(DOCUMENT_LINK)) {
                wikiPage.setLink(line.substring(DOCUMENT_LINK.length(), line.length()).trim());
                continue;
            }
            if (line.startsWith(DOCUMENT_PURPOSE)) {
                wikiPage.setPurpose(line.substring(DOCUMENT_PURPOSE.length(), line.length()).trim());
                continue;
            }


            if (line.startsWith(DOCUMENT_END)) {
                break;
            }

            if (include) {
                unparsedDocumentation.append(line).append("\n");
            }

            if (line.startsWith(DOCUMENT_BEGIN)) {
                include = true;
            }
        }

        wikiPage.setContents(processor.process(unparsedDocumentation.toString()));

        if (include && (wikiPage.getTestcase() == null || wikiPage.getTestcase().isEmpty())) {
            throw new RuntimeException("DOCUMENTATION-TESTCASE has no name " + file.getCanonicalPath());
        }

        if (include) {
            return wikiPage;
        }

        return null;
    }

}