package nl.sodeso.maven.fitnesse.plugin;

import nl.sodeso.commons.fileutils.FileFinder;
import nl.sodeso.commons.fileutils.FileHandler;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
public class CollectFitnessePages {

    private static final String FITNESSE_WIKI_PAGE = "content.txt";

    public static List<File> CollectFitnessePages(String directory) {
        final List<File> files = new ArrayList<>();

        FileFinder.findFilesMatchingExpression(directory, FITNESSE_WIKI_PAGE, new FileHandler() {
            @Override
            public void handle(File file) {
                files.add(file);
            }
        });

        return files;
    }


}
