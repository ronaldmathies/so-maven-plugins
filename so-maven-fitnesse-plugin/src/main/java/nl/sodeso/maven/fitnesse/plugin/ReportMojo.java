package nl.sodeso.maven.fitnesse.plugin;

import freemarker.template.Template;
import freemarker.template.TemplateException;
import nl.sodeso.commons.freemarker.FreemarkerException;
import nl.sodeso.commons.freemarker.FreemarkerUtil;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.model.Resource;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecution;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

/**
 * @author Ronald Mathies
 */
@Mojo(name="report")
public class ReportMojo extends AbstractMojo {

    @Component(role = MavenSession.class)
    protected MavenSession session;

    @Component(role = MojoExecution.class)
    protected MojoExecution execution;

    @Component(role = MavenProject.class)
    protected MavenProject project;

    @Parameter
    private List<String> suites = null;

    private List<File> files = new ArrayList<>();

    private TreeSet<WikiPage> documentationSegments = new TreeSet<>((o1, o2) -> {
        return o1.getTestcase().compareTo(o2.getTestcase());
    });

    public void execute() throws MojoExecutionException {
        try {
            List<Resource> testResources = project.getTestResources();
            for (Resource resource : testResources) {

                if (suites != null && !suites.isEmpty()) {
                    for (String suite : suites) {
                        String directory = resource.getDirectory() + "/" + suite;
                        files.addAll(CollectFitnessePages.CollectFitnessePages(directory));
                    }
                } else {
                    files.addAll(CollectFitnessePages.CollectFitnessePages(resource.getDirectory()));
                }
            }

            processFitnesseWikiPages();
            createIndexFile();
            createHtmlDocumentationDocument();


            for (File file : files) {
                getLog().info("File:" + file.getCanonicalPath());
            }


        } catch (IOException e) {
            getLog().error(e);
        }
    }

    private void processFitnesseWikiPages() throws IOException {
        for (File file : files) {
            WikiPage wikiPage = FitnesseWikiDocumentationRenderer.render(file);
            if (wikiPage != null) {
                documentationSegments.add(wikiPage);
            }
        }
    }

    private void createHtmlDocumentationDocument() {
        try {
            Template index = FreemarkerUtil.loadTemplate("index.ftl");
            Map<Object, Object> dataModel = new HashMap<>();
            dataModel.put("wikipages", documentationSegments);

            index.process(dataModel, new FileWriter(createIndexFile()));
        } catch (FreemarkerException e) {
            getLog().error(e);
        } catch (TemplateException e) {
            getLog().error(e);
        } catch (IOException e) {
            getLog().error(e);
        }
    }

    private File createIndexFile() {
        String buildDirectory = project.getBuild().getDirectory();
        String targetDirectory = buildDirectory + (buildDirectory.endsWith("/") ? "" : "/") + "fitnesse-documentation/";

        File target = new File(targetDirectory);
        if (!target.exists()) {
            if (target.mkdirs()) {
                return new File(targetDirectory + "index.html");
            } else {
                getLog().error("Error creating target folder to write fitnesse documentation '" + targetDirectory + "'.");
            }
        }

        return new File(targetDirectory + "index.html");
    }

}
