package nl.sodeso.maven.fitnesse.plugin;

/**
 * Created by dbxrma on 11-5-2015.
 */
public class WikiPage {

    private String testcase;
    private String link;
    private String purpose;
    private String contents;

    public WikiPage() {}

    public String getTestcase() {
        return testcase;
    }

    public void setTestcase(String testcase) {
        this.testcase = testcase;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }
}
