package nl.sodeso.maven.fitnesse.plugin;

import nl.sodeso.commons.fileutils.FileFinder;
import org.apache.maven.execution.DefaultMavenExecutionRequest;
import org.apache.maven.execution.MavenExecutionRequest;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.model.Resource;
import org.apache.maven.plugin.MojoExecution;
import org.apache.maven.plugin.testing.AbstractMojoTestCase;
import org.apache.maven.project.MavenProject;
import org.apache.maven.project.ProjectBuilder;
import org.apache.maven.project.ProjectBuildingRequest;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

/**
 * @author Ronald Mathies
 */
public class ReportMojoTest extends AbstractMojoTestCase {

    public String basedir = System.getProperty(FileFinder.PROP_USER_DIR);

    @Test
    public void test() throws Exception {
        File pom = getTestFile("/src/test/resources/headers/pom.xml");
        assertNotNull("The file 'pom-datasource.xml' cannot be found.", pom);
        assertTrue("The file 'pom.xml' does not exist.", pom.exists());

        ReportMojo mojo = createMojo(pom);
        addTestResource(mojo, "/src/test/resources/headers/resources");
        setOutputDirectory(mojo, "/target/mojo-test/headers");

        mojo.execute();

        compareTwoFiles(basedir + "/src/test/resources/headers/index.html", basedir + "/target/mojo-test/headers/fitnesse-documentation/index.html");
    }

    private ReportMojo createMojo(final File pom) throws Exception {
        final MavenExecutionRequest executionRequest = new DefaultMavenExecutionRequest();
        final ProjectBuildingRequest buildingRequest = executionRequest.getProjectBuildingRequest();
        final ProjectBuilder projectBuilder = this.lookup(ProjectBuilder.class);
        final MavenProject project = projectBuilder.build(pom, buildingRequest).getProject();

        final MavenSession session = newMavenSession(project);
        final MojoExecution execution = newMojoExecution("report");
        final ReportMojo mojo = (ReportMojo) this.lookupConfiguredMojo(session, execution);

        mojo.session = session;
        mojo.execution = execution;
        mojo.project = project;

        return mojo;
    }

    private void addTestResource(ReportMojo mojo, String path) {
        Resource testResource = new Resource();
        testResource.setDirectory(basedir + path);
        mojo.project.getBuild().setTestResources(Arrays.asList(new Resource[]{ testResource }));
    }

    private void setOutputDirectory(ReportMojo mojo, String path) {
        mojo.project.getBuild().setDirectory(basedir + path);
    }

    private void compareTwoFiles(String file1, String file2) {
        String digestFromFile1 = createDigestFromFile(file1);
        String digestFromFile2 = createDigestFromFile(file2);

        assertEquals(digestFromFile1, digestFromFile2);
    }

    private String createDigestFromFile(String file) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA1");
            FileInputStream fis = new FileInputStream(new File(file));

            byte[] dataBytes = new byte[1024];
            int nread = 0;
            while ((nread = fis.read(dataBytes)) != -1) {
                md.update(dataBytes, 0, nread);
            }

            byte[] mdbytes = md.digest();

            StringBuffer sb = new StringBuffer("");
            for (int i = 0; i < mdbytes.length; i++) {
                sb.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16).substring(1));
            }

            return sb.toString();
        } catch (IOException e) {
            assertNull(e);
        } catch (NoSuchAlgorithmException e) {
            assertNull(e);
        }

        return null;
    }

}
