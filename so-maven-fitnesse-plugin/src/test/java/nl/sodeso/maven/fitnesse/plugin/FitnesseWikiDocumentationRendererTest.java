package nl.sodeso.maven.fitnesse.plugin;

import nl.sodeso.commons.fileutils.FileFinder;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertEquals;

/**
 * @author Ronald Mathies
 */
public class FitnesseWikiDocumentationRendererTest {

    @Test
    public void test() throws Exception {
        String content = System.getProperty(FileFinder.PROP_USER_DIR) + "/src/test/resources/TestSuite/StartApplication/content.txt";
        WikiPage wikiPage = FitnesseWikiDocumentationRenderer.render(new File(content));
        assertEquals("TestSuite.StartApplication", wikiPage.getTestcase());
        assertEquals("www.google.com", wikiPage.getLink());
        assertEquals("Simple testcase for opening the application", wikiPage.getPurpose());
        assertEquals("<hr />\n" +
                "<p>This is a longer description with a bullet list.</p>\n" +
                "<ul>\n" +
                "<li>A</li>\n" +
                "<li>B</li>\n" +
                "<li>C<hr />\n" +
                "</li>\n" +
                "</ul>\n", wikiPage.getContents());
    }
}