package nl.sodeso.maven.fitnesse.plugin;

import nl.sodeso.commons.fileutils.FileFinder;
import org.junit.Test;

import java.io.File;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * @author Ronald Mathies
 */
public class CollectFitnessePagesTest {

    @Test
    public void testCollectFitnessePages() throws Exception {
        List<File> files = CollectFitnessePages.CollectFitnessePages(System.getProperty(FileFinder.PROP_USER_DIR) + "/src/test/resources/TestSuite");

        // Yes 8, 4 from the resources and 4 from the target folder.
        assertEquals("Expected four content.txt files.", 4, files.size());
    }
}