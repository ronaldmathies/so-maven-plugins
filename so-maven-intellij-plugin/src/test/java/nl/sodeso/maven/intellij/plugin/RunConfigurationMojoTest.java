package nl.sodeso.maven.intellij.plugin;

import org.apache.maven.execution.DefaultMavenExecutionRequest;
import org.apache.maven.execution.MavenExecutionRequest;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.model.Resource;
import org.apache.maven.plugin.MojoExecution;
import org.apache.maven.plugin.testing.AbstractMojoTestCase;
import org.apache.maven.project.MavenProject;
import org.apache.maven.project.ProjectBuilder;
import org.apache.maven.project.ProjectBuildingRequest;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

/**
 * @author Ronald Mathies
 */
public class RunConfigurationMojoTest extends AbstractMojoTestCase {

    public String basedir = System.getProperty("user.dir");

    @Test
    public void test() throws Exception {
        File pom = getTestFile("/src/test/resources/runConfigurations/pom.xml");
        assertNotNull("The file 'pom.xml' cannot be found.", pom);
        assertTrue("The file 'pom.xml' does not exist.", pom.exists());

        RunConfigurationMojo mojo = createMojo(pom);
        setOutputDirectory(mojo, "/target/mojo-test");

        mojo.execute();

        compareTwoFiles(basedir + "/src/test/resources/runconfigurations/Maven_Run_Configuration_1.xml", basedir + "/target/mojo-test/.idea/runConfigurations/Maven_Run_Configuration_1.xml");
        compareTwoFiles(basedir + "/src/test/resources/runconfigurations/Maven_Run_Configuration_2.xml", basedir + "/target/mojo-test/.idea/runConfigurations/Maven_Run_Configuration_2.xml");
        compareTwoFiles(basedir + "/src/test/resources/runconfigurations/GWT_Run_Configuration_1.xml", basedir + "/target/mojo-test/.idea/runConfigurations/GWT_Run_Configuration_1.xml");
        compareTwoFiles(basedir + "/src/test/resources/runconfigurations/GWT_Run_Configuration_2.xml", basedir + "/target/mojo-test/.idea/runConfigurations/GWT_Run_Configuration_2.xml");

    }

    private RunConfigurationMojo createMojo(final File pom) throws Exception {
        final MavenExecutionRequest executionRequest = new DefaultMavenExecutionRequest();
        final ProjectBuildingRequest buildingRequest = executionRequest.getProjectBuildingRequest();
        final ProjectBuilder projectBuilder = this.lookup(ProjectBuilder.class);
        final MavenProject project = projectBuilder.build(pom, buildingRequest).getProject();

        final MavenSession session = newMavenSession(project);
        final MojoExecution execution = newMojoExecution("generate");
        final RunConfigurationMojo mojo = (RunConfigurationMojo) this.lookupConfiguredMojo(session, execution);

        mojo.session = session;
        mojo.execution = execution;
        mojo.project = project;

        return mojo;
    }
//komkommer, brood, melk, ijsje, fuji appel

    private void addTestResource(RunConfigurationMojo mojo, String path) {
        Resource testResource = new Resource();
        testResource.setDirectory(basedir + path);
        mojo.project.getBuild().setTestResources(Arrays.asList(new Resource[]{ testResource }));
    }

    private void setOutputDirectory(RunConfigurationMojo mojo, String path) {
        mojo.project.getBuild().setDirectory(basedir + path);
    }

    private void compareTwoFiles(String file1, String file2) {
        String digestFromFile1 = createDigestFromFile(file1);
        String digestFromFile2 = createDigestFromFile(file2);

        assertEquals(digestFromFile1, digestFromFile2);
    }

    private String createDigestFromFile(String file) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA1");
            FileInputStream fis = new FileInputStream(new File(file));

            byte[] dataBytes = new byte[1024];
            int nread = 0;
            while ((nread = fis.read(dataBytes)) != -1) {
                md.update(dataBytes, 0, nread);
            }

            byte[] mdbytes = md.digest();

            StringBuffer sb = new StringBuffer("");
            for (int i = 0; i < mdbytes.length; i++) {
                sb.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16).substring(1));
            }

            return sb.toString();
        } catch (IOException e) {
            assertNull(e);
        } catch (NoSuchAlgorithmException e) {
            assertNull(e);
        }

        return null;
    }

}
