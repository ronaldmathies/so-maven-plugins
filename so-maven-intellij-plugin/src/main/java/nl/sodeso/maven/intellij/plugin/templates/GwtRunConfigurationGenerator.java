package nl.sodeso.maven.intellij.plugin.templates;

import freemarker.template.Template;
import freemarker.template.TemplateException;
import nl.sodeso.commons.freemarker.FreemarkerException;
import nl.sodeso.commons.freemarker.FreemarkerUtil;
import nl.sodeso.maven.intellij.plugin.Gwt;
import nl.sodeso.maven.intellij.plugin.MavenPluginException;
import org.apache.maven.project.MavenProject;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Ronald Mathies
 */
public class GwtRunConfigurationGenerator extends AbstractRunConfigurationGenerator {

    private Gwt gwt = null;

    public GwtRunConfigurationGenerator(MavenProject project, Gwt gwt) {
        super(project);

        this.gwt = gwt;
    }

    public void process() throws MavenPluginException {
        try {
            Template template = FreemarkerUtil.loadTemplate("gwt-run-configuration.ftl");

            Map<Object, Object> dataModel = new HashMap<>();
            dataModel.put("label", gwt.getLabel());
            dataModel.put("module", gwt.getModule());
            dataModel.put("vmOptions", gwt.getVmOptions());
            dataModel.put("devModeParameters", gwt.getDevModeParameters());
            dataModel.put("enableJavascriptDebugger", gwt.isEnableJavascriptDebugger());
            dataModel.put("useSuperDevMode", gwt.isUseSuperDevMode());

            template.process(dataModel, new FileWriter(createRunConfigurationFile(gwt.getLabel())));
        } catch (FreemarkerException e) {
            throw new MavenPluginException(e.getMessage());
        } catch (IOException e) {
            throw new MavenPluginException(e.getMessage());
        } catch (TemplateException e) {
            throw new MavenPluginException(e.getMessage());
        }
    }


}
