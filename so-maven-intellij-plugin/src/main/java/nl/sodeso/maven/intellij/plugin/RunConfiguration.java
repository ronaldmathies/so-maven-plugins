package nl.sodeso.maven.intellij.plugin;

/**
 * @author Ronald Mathies
 */
public class RunConfiguration {

    private Maven maven = null;
    private Gwt gwt = null;

    public RunConfiguration() {}

    public Maven getMaven() {
        return maven;
    }

    public void setMaven(Maven maven) {
        this.maven = maven;
    }

    public Gwt getGwt() {
        return gwt;
    }

    public void setGwt(Gwt gwt) {
        this.gwt = gwt;
    }
}
