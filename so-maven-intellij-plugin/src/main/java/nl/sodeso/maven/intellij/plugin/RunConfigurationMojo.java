package nl.sodeso.maven.intellij.plugin;

import nl.sodeso.maven.intellij.plugin.templates.GwtRunConfigurationGenerator;
import nl.sodeso.maven.intellij.plugin.templates.MavenRunConfigurationGenerator;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecution;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

import java.util.List;

/**
 * @author Ronald Mathies
 */
@Mojo(name="generate")
public class RunConfigurationMojo extends AbstractMojo {

    @Component(role = MavenSession.class)
    protected MavenSession session;

    @Component(role = MojoExecution.class)
    protected MojoExecution execution;

    @Component(role = MavenProject.class)
    protected MavenProject project;

    @Parameter
    private List<RunConfiguration> runConfigurations;

    public void execute() throws MojoExecutionException {
        try {
            for (RunConfiguration runConfiguration : runConfigurations) {
                if (runConfiguration.getMaven() != null) {
                    new MavenRunConfigurationGenerator(project, runConfiguration.getMaven()).process();
                }
                if (runConfiguration.getGwt() != null) {
                    new GwtRunConfigurationGenerator(project, runConfiguration.getGwt()).process();;
                }
            }
        } catch (MavenPluginException e) {
            getLog().error(e);
        }
    }
}
