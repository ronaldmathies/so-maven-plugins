package nl.sodeso.maven.intellij.plugin;

/**
 * @author Ronald Mathies
 */
public class MavenPluginException extends Exception {

    public MavenPluginException(String message) {
        super(message);
    }

}
