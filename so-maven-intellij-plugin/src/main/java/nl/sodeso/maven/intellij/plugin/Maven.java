package nl.sodeso.maven.intellij.plugin;

import java.util.List;

/**
 * @author Ronald Mathies
 */
public class Maven {

    private String label = null;
    private List<String> goals;
    private List<String> profiles;
    private String module;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public List<String> getGoals() {
        return goals;
    }

    public void setGoals(List<String> goals) {
        this.goals = goals;
    }

    public List<String> getProfiles() {
        return profiles;
    }

    public void setProfiles(List<String> profiles) {
        this.profiles = profiles;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }
}
