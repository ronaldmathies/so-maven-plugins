package nl.sodeso.maven.intellij.plugin;

import org.apache.maven.project.MavenProject;

/**
 * @author Ronald Mathies
 */
public class Environment {

    public static String getOutputDirectory(MavenProject project) {
        if (project.getName().equalsIgnoreCase("so-maven-intellij-plugin-test")) {
            return project.getBuild().getDirectory();
        } else {
            return project.getBasedir().getAbsolutePath();
        }
    }

}
