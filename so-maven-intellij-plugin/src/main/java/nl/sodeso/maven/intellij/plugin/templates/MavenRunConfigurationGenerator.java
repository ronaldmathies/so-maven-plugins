package nl.sodeso.maven.intellij.plugin.templates;

import freemarker.template.Template;
import freemarker.template.TemplateException;
import nl.sodeso.commons.freemarker.FreemarkerException;
import nl.sodeso.commons.freemarker.FreemarkerUtil;
import nl.sodeso.maven.intellij.plugin.Maven;
import nl.sodeso.maven.intellij.plugin.MavenPluginException;
import org.apache.maven.project.MavenProject;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Ronald Mathies
 */
public class MavenRunConfigurationGenerator extends AbstractRunConfigurationGenerator {

    private Maven maven = null;

    public MavenRunConfigurationGenerator(MavenProject project, Maven maven) {
        super(project);

        this.maven = maven;
    }

    public void process() throws MavenPluginException {
        try {
            Template template = FreemarkerUtil.loadTemplate("maven-run-configuration.ftl");

            Map<Object, Object> dataModel = new HashMap<>();
            dataModel.put("label", maven.getLabel());
            dataModel.put("goals", maven.getGoals());
            dataModel.put("profiles", maven.getProfiles());
            dataModel.put("module", maven.getModule());
            template.process(dataModel, new FileWriter(createRunConfigurationFile(maven.getLabel())));
        } catch (FreemarkerException e) {
            throw new MavenPluginException(e.getMessage());
        } catch (IOException e) {
            throw new MavenPluginException(e.getMessage());
        } catch (TemplateException e) {
            throw new MavenPluginException(e.getMessage());
        }
    }


}
