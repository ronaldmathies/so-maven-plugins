package nl.sodeso.maven.intellij.plugin.templates;

import nl.sodeso.maven.intellij.plugin.Environment;
import nl.sodeso.maven.intellij.plugin.MavenPluginException;
import org.apache.maven.project.MavenProject;

import java.io.File;

/**
 * @author Ronald Mathies
 */
public abstract class AbstractRunConfigurationGenerator {

    private MavenProject project;

    public AbstractRunConfigurationGenerator(MavenProject project) {
        this.project = project;
    }

    public File createRunConfigurationFile(String label) throws MavenPluginException {
        String projectBaseDir = Environment.getOutputDirectory(project);
        String targetDirectory = projectBaseDir + (projectBaseDir.endsWith("/") ? "" : "/") + ".idea/runConfigurations/";

        File target = new File(targetDirectory);
        if (!target.exists()) {
            if (!target.mkdirs()) {
                throw new MavenPluginException("Failed to create target .idea folder.");
            }
        }

        return new File(targetDirectory + label.replaceAll(" ", "_") + ".xml");
    }

    public MavenProject getProject() {
        return this.project;
    }

}
