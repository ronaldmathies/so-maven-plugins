package nl.sodeso.maven.intellij.plugin;

/**
 * @author Ronald Mathies
 */
public class Gwt {

    private String label;
    private String module;
    private String vmOptions;
    private String devModeParameters;
    private Boolean enableJavascriptDebugger = false;
    private Boolean useSuperDevMode = true;

    public Gwt() {}

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public String getVmOptions() {
        return vmOptions;
    }

    public void setVmOptions(String vmOptions) {
        this.vmOptions = vmOptions;
    }

    public String getDevModeParameters() {
        return devModeParameters;
    }

    public void setDevModeParameters(String devModeParameters) {
        this.devModeParameters = devModeParameters;
    }

    public Boolean isEnableJavascriptDebugger() {
        return enableJavascriptDebugger;
    }

    public void setEnableJavascriptDebugger(Boolean enableJavascriptDebugger) {
        this.enableJavascriptDebugger = enableJavascriptDebugger;
    }

    public Boolean isUseSuperDevMode() {
        return useSuperDevMode;
    }

    public void setUseSuperDevMode(Boolean useSuperDevMode) {
        this.useSuperDevMode = useSuperDevMode;
    }
}
