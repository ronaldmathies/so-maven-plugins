<component name="ProjectRunConfigurationManager">
  <configuration default="false" name="${label}" type="MavenRunConfiguration" factoryName="Maven">
    <MavenSettings>
      <option name="myGeneralSettings" />
      <option name="myRunnerSettings" />
      <option name="myRunnerParameters">
        <MavenRunnerParameters>
          <option name="profiles">
            <set />
          </option>
          <option name="goals">
            <list>
<#list goals as goal>
              <option value="${goal}" />
</#list>
            </list>
          </option>
          <option name="profilesMap">
            <map>
<#list profiles as profile>
              <entry key="${profile}" value="true" />
</#list>
            </map>
          </option>
          <option name="resolveToWorkspace" value="false" />
          <option name="workingDirPath" value="$PROJECT_DIR$/${module}" />
        </MavenRunnerParameters>
      </option>
    </MavenSettings>
    <method />
  </configuration>
</component>