<component name="ProjectRunConfigurationManager">
  <configuration default="false" name="${label}" type="GWT.ConfigurationType" factoryName="GWT Configuration">
    <module name="${module}" />
    <option name="VM_PARAMETERS" value="${vmOptions}" />
    <option name="SHELL_PARAMETERS" value="${devModeParameters}" />
    <option name="RUN_PAGE" value="index.html" />
    <option name="START_JAVASCRIPT_DEBUGGER" value="${enableJavascriptDebugger?c}" />
    <option name="USE_SUPER_DEV_MODE" value="${useSuperDevMode?c}" />
    <method />
  </configuration>
</component>